#!/bin/bash

NUMERODECICLO=0
Movep=$BINDIR"./movep.sh"
Logep=$BINDIR"./logep.sh"


#funcion que se fija si es un archivo de texto
function archivo_de_texto {

		if [ ${1: -4} != ".csv" ]
		  then
				$Logep "deamonep" "INFO" "Archivo rechazado, motivo: no es archivo de texto"
				return 0
		  fi

			return 1

}

function archivo_no_vacio {

		  if ! [ -s $1 ]
		  then
				$Logep "deamonep" "INFO" "Archivo rechazado, motivo: archivo vacio"
				return 0
		  fi

			return 1

}

function archivo_formato_correcto {

		  if ! [[ "${1:0:-4}" =~ [a-z]{9}+_+[0-9]{4}+_+[0-9]{1,2}+_+[0-9]{8} ]]
		  then
				$Logep "deamonep" "INFO" "Archivo rechazado, motivo: formato de nombre incorrecto"
				return 0
		  fi

			return 1

}

function archivo_anio_correcto {

		  if [ "${1:10:4}" != $(date +"%Y") ]
		  then
				$Logep "deamonep" "INFO" "Archivo rechazado, motivo: año "${1:10:4}" incorrecto"
				return 0
		  fi

			return 1

}

function archivo_provincia_correcta {

	local Provs="$DIRMAE/provincias.csv"
	if [[ ${#1} -eq 29 ]] ; then
		provi=1
	else
		provi=2
	fi

	local existeLaProv=$(grep -c "${1:15:$provi}" "$Provs")

	if [ $existeLaProv == 0 ] ; then
		$Logep "deamonep" "INFO" "Archivo rechazado, motivo: provincia "${1:15:$provi}" incorrecta"
		return 0
	fi

	return 1

}

function archivo_fecha_correcta {

	# local aniomesdia=${1: -12:-4}
	# if ! ([ "${aniomesdia:0:4}" == "2016" ] && [ ${aniomesdia:4:2} -lt 13 ] &&
	# 		[ ${aniomesdia:6:2} -lt 32 ] && [ $aniomesdia -ge 20160101 ]) ; then
	# 	$Logep "deamonep" "INFO" "Archivo rechazado, motivo: fecha $aniomesdia incorrecta"
	# 	return 0
	# fi

	local aniomesdia=${1: -12:-4}
	local mesdiaanio="${aniomesdia:4:2}/${aniomesdia:6:2}/${aniomesdia:0:4}"

	[[ $(date -d "${mesdiaanio//-/\/}" 2> /dev/null) ]] && ret=1 || ret=0

	if [ $ret = 0 ] ; then
 		$Logep "deamonep" "INFO" "Archivo rechazado, motivo: fecha $aniomesdia incorrecta"
	fi

	return $ret

}

function intentar_llamar_procep {
	# pid=`pgrep 'procep.sh'`
	# if [ $? -eq 0 ]
	# then
	# 	echo "Invocacion de Procep pospuesta para el siguiente ciclo."
	# else
	# 	$BINDIR"./procep.sh"&
	# 	pid=`pgrep 'procep.sh'`
	# 	echo "Procep corriendo bajo el no.: ${pid}."
	# fi
	echo "llamando procep"
}

echo "********* INICIANDO DEAMONEP **************"
while [ 1 ]
do

NUMERODECICLO=`expr $NUMERODECICLO + 1`
#echo "deamonep ciclo nro. $NUMERODECICLO" #LLAMAR A LOGEPs
$Logep "deamonep" "INFO" "deamonep ciclo nro. $NUMERODECICLO"

archivos=`ls $DIRREC`
rechazados="$DIRNOK"
aceptados="$DIROK"

if [ -n "$archivos" ]
then
	for nombre in $archivos
	do
		#echo "Archivo detectado: $nombre" #LLAMAR A LOGEPs
		$Logep "deamonep" "INFO" "Archivo detectado: $nombre"

		mi_arch="$DIRREC/$nombre"

		#CHEQUEO NOMBRE
		archivo_de_texto "$mi_arch"

		if [[ "$?" -eq 0 ]]; then
			$Movep "$mi_arch" "$rechazados" "deamonep"
			continue
		fi #si no es valido salgo del loop

		#CHEQUEO NO VACIO
		archivo_no_vacio "$mi_arch"

		if [[ "$?" -eq 0 ]]; then
			$Movep "$mi_arch" "$rechazados" "deamonep"
			continue
		fi #si no es valido salgo del loop

		#CHEQUEO FORMATO
		archivo_formato_correcto "$nombre"

		if [[ "$?" -eq 0 ]]; then
			$Movep "$mi_arch" "$rechazados" "deamonep"
			continue
		fi #si no es valido salgo del loop

		#año corriente
		archivo_anio_correcto "$nombre"

		if [[ "$?" -eq 0 ]]; then
			$Movep "$mi_arch" "$rechazados" "deamonep"
			continue
		fi #si no es valido salgo del loop

		#prov correcta
		archivo_provincia_correcta "$nombre"

		if [[ "$?" -eq 0 ]]; then
			$Movep "$mi_arch" "$rechazados" "deamonep"
			continue
		fi #si no es valido salgo del loop

		#fecha correcta
		archivo_fecha_correcta "$nombre"

		if [[ "$?" -eq 0 ]]; then
			$Movep "$mi_arch" "$rechazados" "deamonep"
			continue
		fi #si no es valido salgo del loop

		$Logep "deamonep" "INFO" "Archivo aceptado"
		$Movep "$mi_arch" "$aceptados" "deamonep"

		#intentar_llamar_procep

	done
fi

sleep 5s
done
