#!/bin/bash

GRUPO=$(pwd)
WHERE="$1"; # nombre del comando o funcion  que genera el mensaje
WHY="$2"; # es el mensaje

if [[ "$#" -eq 3 ]]; then	# Si hay 3 parametros significa que se identifica el tipo de error.
	WHAT="$3"				# Tipo de mensaje: INFO, WARN o ERROR
elif [[ "$#" -eq 2 ]]; then	# Si hay 2 parametros unicamente se toma la variable WHAT = INFO por default.
	WHAT="INFO"				# Tipo de error: INFO por default.
else
	bash logep.sh "logep" "Recibidos $# parametros en vez de 2 o 3" "ERROR"
	exit $#
fi


#harcodeo  esto hasta que aparesca ayee haga la var de entorno con la direccion relativa correspondiente
if [ "$WHERE"  = "Instalep" ]; then
	DIRECCIONLOG="$GRUPO/dirconf/$WHERE.log"
else
	DIRECCIONLOG="$DIRLOG/$WHERE.log"
fi


MENSAJE=$(date +"%y%m%d") # concateno toda la informacion del mensaje
MENSAJE="$MENSAJE "$(date +"%T")
MENSAJE="$MENSAJE-$USER"
MENSAJE="$MENSAJE-$WHERE-$WHY-$WHAT"

if [ -a $DIRECCIONLOG ]; then
		#echo "existe"
		 echo  $MENSAJE >> $DIRECCIONLOG
	else
		#echo "no existe"
		 echo $MENSAJE > $DIRECCIONLOG
fi

TAMANIO_bytes=`stat -c %s "$DIRECCIONLOG"` 	#Comando que indica el tamaño de un archivo. Devuelve el tamaño en bytes.
#Paso de bytes a kb.
TAMANIO_kb=$(($TAMANIO_bytes/1024))
# echo "KiloBytes: $TAMANIO_kb"


if [[ "$TAMANIO_kb" -ge 50 ]]; then		#Si el tamaño del archivo es mayor que el LOGSIZE
	TEMPORAL='templog.log' 						#Creo un archivo temporal para el log nuevo.
	echo "------------------------------Log Excedido------------------------------" >> "$TEMPORAL" 		#Agrego como primera linea "Log Excedido" para indicar que se realizo este procedimiento.
	tail -n 50 "$DIRECCIONLOG" >> "$TEMPORAL" 		#Agrego las ultimas 50 lineas del log viejo al nuevo.
	rm "$DIRECCIONLOG"								#Elimino el viejo log
	mv "$TEMPORAL" "$DIRECCIONLOG"					#Cambia el nombre del archivo de templog al original.
fi
