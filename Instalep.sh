#!/bin/bash

# INITIAL VARIABLES:
CURRENT_USER=$(whoami)
GRUPO=$(pwd)

CONF_DIR_NAME="dirconf"
CONF_FILE_NAME="Instalep.conf"

CONF_FILE=${GRUPO}/${CONF_DIR_NAME}/${CONF_FILE_NAME}
TEMP_CONF_FILE=${GRUPO}/${CONF_FILE_NAME}.temp
    
# (!) allways init or load the helper modules
    
function initHelperModules {
    Logger=${GRUPO}/./logep.sh
    Movep=${GRUPO}/./movep.sh
}

function loadHelperModules {
    local dirBin
    readVarFromConfiguration "DIRBIN" dirBin ${CONF_FILE}
    
    Logger=${GRUPO}/${dirBin}/./logep.sh
    Movep=${GRUPO}/${dirBin}/./movep.sh
}

function checkIfFileExist {
file=$1
dir=$2
	if [ ! -f $file ]; then
		if [ ! -f "$dir"/$file ]; then
			echo -e "No existe el archivo $file" 
            echo -e "El archivo es necesario para la instalacion;"
            echo -e "Asegurese que el mismo se encuentre localizado sobre ${GRUPO} y vuelva a instalar"
			echo -e "Instalación CANCELADA!"
            exit
		fi
	fi
}

function checkIfMasterFilesExist {
	checkIfFileExist "centros.csv" ${GRUPO};
	checkIfFileExist "provincias.csv" ${GRUPO};
    checkIfFileExist "trimestres.csv" ${GRUPO};
}

function checkIfBinFilesExist {
	checkIfFileExist "logep.sh" ${GRUPO};
	checkIfFileExist "movep.sh" ${GRUPO};
    checkIfFileExist "initep.sh" ${GRUPO};
    checkIfFileExist "deamonep.sh" ${GRUPO};
	checkIfFileExist "procep.sh" ${GRUPO};
	checkIfFileExist "listep.sh" ${GRUPO};
	checkIfFileExist "start_deamonep.sh" ${GRUPO};
	checkIfFileExist "stop_deamonep.sh" ${GRUPO};	
}

# fake map: (currentPath - label - variableCode)
directories=(
    "DIRBIN"    "ejecutables"
    "DIRMAE"    "Maestros-y-Tablas"
    "DIRREC"    "recepción-de-novedades"
    "DIROK"     "Archivos-Aceptados"
    "DIRPROC"   "Archivos-Procesados"
    "DIRINFO"   "Reportes"
    "DIRLOG"    "log"
    "DIRNOK"    "rechazados"
)

# AUXLIAR FUNCTIONS

function writeMessage {
    local msg=${1}
    local status=${2}

    echo -e ${msg}
    $Logger Instalep "${msg}" ${status}
    return 0
}

# required: expects a parameter for save the user valid confirmation
# if the parameter didn't exist, the function throw an error
function getUserConfirmation {
    local confirmation=${1} # use to return the valid confirmation
    local confirmMessage=${2}
    local isValid=0
    local input
    while [ $isValid -eq 0 ]; do
        writeMessage "${confirmMessage} (s/n):" "INFO"
        read input
        $Logger Instalep "${input}" "INFO"
		if ! [ -z ${input} ]; then
            if [ ${input} = "s" ] || [ ${input} = "n" ]; then
                isValid=1
                eval $confirmation=${input} # assign valid confirmation
            else
                writeMessage "Valor Inválido" "INFO"
			fi
		fi
	done
    return 0
}

# TO DO: add more validations (special characters, //, =,  what happends if put user??
function isValidDirectoryName {
    local directoryName=${1}
    local currentDirectory=${2}
    local existingPath=${CONF_DIR_NAME}

    if [[ ${directoryName} =~ .*//.* ]] || [[ ${directoryName} =~ .*[=|[|]|(|)].* ]]; then
        writeMessage "Error: los caracteres '//' , '=' , '[' , ']' , '(' y ')' estan prohibidos para nombrar directorios" "INFO"
        return 1 
    fi

    if [ ${directoryName} = ${existingPath} ]; then
        writeMessage "Error: La ruta '${GRUPO}/${directoryName}' esta reservada por el sistema" "INFO"
        return 1
    fi
    # be careful with inner loops!(don't repeat iterator variable name)
    for ((it=0 ; it < ${#directories[@]} ; it+=2)); do
        readVarFromConfiguration ${directories[it]} existingPath ${TEMP_CONF_FILE}
        if [ ${currentDirectory} = ${directories[it]} ]; then #if it's the same there is not a problem
            return 0 #iterate until itself
        fi
        if [ ${directoryName} = ${existingPath} ]; then
            writeMessage "Error: La ruta '${GRUPO}/${directoryName}' ya esta sinedo utilizada por otro directorio" "INFO"
            return 1
        fi
    done
    return 0
}

# TO DO: make more nicely! ;P
function readVarFromConfiguration {
    local filePath=${3}
    local variableCode=${1}
    local result=${2}
    local value=$(grep -o "${variableCode}=.*=${CURRENT_USER}" ${filePath}) # fix: stop in the first occurence
    value=$(echo ${value} | cut -f2 -d"=")
    eval $result=${value}

    # Ojo si loggeas antes de encontrar el log pincha (cuando corres instalep una vez ya instalado)
    #$Logger Instalep "lectura sobre ${filePath} - ${variableCode}: \n
    #                 resultado: ${value} \n" "INFO"
}

function writeVarInConfigutation {
    local filePath=${3}
    local variableCode=${1}
    local newValue=${2}
    local lastValue
    readVarFromConfiguration ${variableCode} lastValue ${filePath}
    sed -i "s|${variableCode}=${lastValue}|${variableCode}=${newValue}|g" ${filePath}

    $Logger Instalep "escritura sobre ${filePath} - ${variableCode}: \n
                     old value: ${lastValue} \n
                     new value: ${newValue} \n" "INFO"
}

function initConfigurationTemp {
	local date=`date +"%D %r"`
    echo -e GRUPO=${GRUPO}=${CURRENT_USER}=${date}\ >> ${TEMP_CONF_FILE}
    echo -e DIRBIN=bin=${CURRENT_USER}=${date}\ >> ${TEMP_CONF_FILE}
    echo -e DIRMAE=mae=${CURRENT_USER}=${date}\ >> ${TEMP_CONF_FILE}
    echo -e DIRREC=nov=${CURRENT_USER}=${date}\ >> ${TEMP_CONF_FILE}
    echo -e DATASIZE=100=${CURRENT_USER}=${date}\ >> ${TEMP_CONF_FILE}
    echo -e DIROK=ok=${CURRENT_USER}=${date}\ >> ${TEMP_CONF_FILE}
    echo -e DIRPROC=imp=${CURRENT_USER}=${date}\ >> ${TEMP_CONF_FILE}
    echo -e DIRINFO=rep=${CURRENT_USER}=${date}\ >> ${TEMP_CONF_FILE}
    echo -e DIRLOG=log=${CURRENT_USER}=${date}\ >> ${TEMP_CONF_FILE}
    echo -e DIRNOK=nok=${CURRENT_USER}=${date}\ >> ${TEMP_CONF_FILE}
}

function showDirectories {
    local filePath=${1}
    local path
    local label
    writeMessage "Directorio: Resumen" "INFO"
    writeMessage ""
    for ((i=0 ; i < ${#directories[@]} ; i+=2)); do
        readVarFromConfiguration ${directories[i]} path ${filePath}
        label=${directories[i+1]}
        writeMessage "Directorio de ${label//-/ }: '${GRUPO}/${path}'" "INFO"
    done
    writeMessage "" "INFO"
}

function configDirectory {
    local directoryCode=${1}
    local label="${2//-/ }" # (!) Remove spaces!!!
    local actualPath
    local isCorrect=1 # why run only ones?
    local inputPath
    while ! [ $isCorrect -eq 0 ]; do
        isCorrect=0
        readVarFromConfiguration ${directoryCode} actualPath ${TEMP_CONF_FILE}
        writeMessage "Defina el directorio de ${label} (${GRUPO}/${actualPath}):" "INFO"
        read inputPath
        $Logger Instalep "${inputPath}" "INFO"
        if ! [ -z ${inputPath} ]; then
            isValidDirectoryName ${inputPath} ${directoryCode}
            isCorrect=$?
        else
            inputPath=${actualPath}
        fi
    done
    $Logger Instalep "${GRUPO}/${inputPath}" "INFO"
    writeVarInConfigutation ${directoryCode} ${inputPath} ${TEMP_CONF_FILE}
}

function setDirectories {
    local completeSteps=0
    for ((i=0 ; i < ${#directories[@]} ; i+=2)); do
        configDirectory ${directories[i]} ${directories[i+1]}
        completeSteps=$(($completeSteps + 1))
        writeMessage "Directorio: ${completeSteps}/8 completado" "INFO"
    done
}

# FIX ME!!!
function defineDirectoryDiscSpaceForNOV {
    readVarFromConfiguration "DATASIZE" DATASIZE ${TEMP_CONF_FILE}
	local enoughSpace=0;
    local freeSpace=$(df -m . | tail -1 | awk '{print $4}') 
	while [ $enoughSpace -eq 0 ]; do
        writeMessage "Defina espacio mínimo libre para la recepción de archivos en Mbytes (${DATASIZE}):" "INFO"
        read space
        $Logger Instalep "${space}" "INFO"
        if ! [ -z ${space} ]; then
            local isANumber='^[0-9]+$'
			while ! [[ ${space} =~ ${isANumber} ]] || [ ${space} -eq 0 ]; do
				writeMessage "Monto Inválido. Defina nuevamente el espacio minimo en Mbyte:" "INFO"
				read space
				$Logger Instalep "${space}" "INFO"
			done
            DATASIZE=$space;
        fi
        writeVarInConfigutation "DATASIZE" ${DATASIZE} ${TEMP_CONF_FILE}

	    if [ ${freeSpace} -lt ${DATASIZE} ]; then
	    	writeMessage "Insuficiente espacio en disco. \n
                          Espacio disponible: ${freeSpace} Mb. \n
                          Espacio requerido: ${DATASIZE} Mb. \n
                          Intentelo Nuevamente." "INFO"
	    else
	    	enoughSpace=1;
	    fi
	done

    writeMessage "Suficiente espacio en disco. \n
                  Espacio disponible: ${freeSpace} Mb. \n
                  Espacio requerido: ${DATASIZE} Mb. \n
                  De enter para continuar." "INFO"

    # Press "enter" to finish
    local enter
    read enter
}

function defineDirectories {
    local confirm="n"
    while [ $confirm = "n" ]; do
        writeMessage "Directorio: Definir los nombres de los directorios" "INFO"
        writeMessage "-------------------------------------------------- \n" "INFO"
        setDirectories
        writeMessage "Directorio: Finalizado \n" "INFO"
        defineDirectoryDiscSpaceForNOV
        showDirectories ${TEMP_CONF_FILE}
        writeMessage "Estado de la instalación: LISTA" "INFO"
        getUserConfirmation confirm "Desea continuar con la instalación?" "INFO"
        # if [$confirm = "n"]; then
            # clear console!!!
        # fi
    done
    # saveDirectories # extract from the map and save in the temp file!
}

function createDirectoryStructure {
    writeMessage "Creando Estructuras de directorio..." "INFO"
    local path
    for ((i=0 ; i < ${#directories[@]} ; i+=2)); do
        readVarFromConfiguration ${directories[i]} path ${CONF_FILE}
        $(mkdir -p ${path})
        writeMessage "${GRUPO}/${path} ok!" "INFO"
    done
    writeMessage "Estructuras creadas ok! \n" "INFO"
}

function installMasterFilesAndTables {
  writeMessage "Instalando Archivos Maestros y tablas.." "INFO"
  local masterFilesAndTables=`ls *.csv`
  local masterDirectory
  readVarFromConfiguration "DIRMAE" masterDirectory ${CONF_FILE}
  for file in $masterFilesAndTables ; do
    $Movep ${GRUPO}/${file} ${masterDirectory}/
  done
  writeMessage "Archivos Maestros y tablas ok!" "INFO"
}

function moveSensitivesPrograms {
  writeMessage "Instalando Binarios Sensibles.." "INFO"
  local binDirectory
  readVarFromConfiguration "DIRBIN" binDirectory ${CONF_FILE}

  $Movep ${GRUPO}/logep.sh ${binDirectory}/
  Logger=${GRUPO}/${binDirectory}/./logep.sh

  $Movep ${GRUPO}/movep.sh ${binDirectory}/
  Movep=${GRUPO}/${binDirectory}/./movep.sh
  writeMessage "Binarios Sensibles ok!" "INFO"
}

function installPrograms {
  writeMessage "Instalando Programas y funciones.." "INFO"

  moveSensitivesPrograms

  local binaries=`ls *.sh`
  local binDirectory
  readVarFromConfiguration "DIRBIN" binDirectory ${CONF_FILE}
  for file in $binaries ; do
    if [ $file != "Instalep.sh" ]; then
      $Movep ${GRUPO}/${file} ${binDirectory}/
      writeMessage "${GRUPO}/${file} oK!" "INFO"
    fi
  done
  writeMessage "Programas y funciones ok! \n" "INFO"
}

function install {
    createDirectoryStructure
    installMasterFilesAndTables
    installPrograms
}

function saveConfigurationFile {
    writeMessage "Actualizando la configuración del sistema.." "INFO"
    cp -a -p ${TEMP_CONF_FILE} ${CONF_FILE}
    writeMessage "Actualización ok! \n" "INFO"
}

function cleanInstall {
    rm ${TEMP_CONF_FILE}
}

function welcomeMessage {
    writeMessage "------------------------------" "INFO"
    writeMessage "Instalación del Sistema EPLAM:" "INFO"
    writeMessage "------------------------------ \n" "INFO" 
}

# (!) allways init or load the helper modules
if ! [ -e $CONF_FILE ]; then 
    checkIfBinFilesExist # this can exit the installation
    checkIfMasterFilesExist #  this can exit the installation
    initHelperModules
    welcomeMessage
    initConfigurationTemp
    defineDirectories
    saveConfigurationFile
    install
    writeMessage "Instalación CONCLUIDA." "INFO"
    cleanInstall
    writeMessage "Fin del proceso. ${CURRENT_USER} `date +"%D %r"` \n" "INFO"
else
    loadHelperModules
    welcomeMessage
    writeMessage "La instalación ya fue realizada con la siguiente configuración: \n" "INFO"
    showDirectories ${CONF_FILE}
fi
