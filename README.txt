[75.08 - Sistemas Operativos] [FIUBA] [Grupo 5] [2do cuatrimestre 2016] 

*Guardar en el archivo comprimido en un directorio a elecci�n

*Para descomprimir el paquete posici�nese en el directorio elegido anteriormente y ejecute
$tar -xvzf grupo5-tp-EPLAM-2do2016-7508.tgz [-C /ruta/destino]
(En caso de NO proporcionar un ruta de destino, el paquete sera descomprimido en el directorio donde se encuentra el mismo)

*Luego de la descompresi�n se generara un directorio dirconf 

*Para comenzar la instalaci�n ejecute 
$./Instalep.sh
(De no tener los permisos necesarios ejecute chmod +x Instalep.sh y vuelva a intentar)

*Seguir las instrucciones que figuran en pantalla, a lo largo de la instalaci�n se pedir� que defina los distintos directorios para la ejecuci�n del programa.
Si no ingresa ninguno el sistema tomara por default los directorios indicados entre par�ntesis 

Restricciones: ninguno de los directorios debe llevar el nombre "dirconf", el instalador no se lo permitir�.

*Luego de la instalaci�n se crearan todos los directorios proporcionados anteriormente y adem�s se reubicar�n los fuentes y el resto de los archivos.

*Para comenzar la ejecuci�n del sistema ir a al directorio donde est�n los archivos binarios y ejecutar 
$source ./initep.sh

(Tip: En caso de no tener los permisos necesarios ejecute el siguiente comando: $chmod +x initep.sh y vuelva a intentar)

*El initep proporciona la opci�n de arrancar el comando deamon de forma automatica.
Para la ejecuci�n de forma manual ejecute:
$./start_deamonep.sh

Para detenerlo de forma manual ejecute:
$./stop_deamonep.sh

*Es indispensable la correcta instalaci�n e inicializaci�n del sistema antes de seguir con la ejecuci�n de cualquier comando.
