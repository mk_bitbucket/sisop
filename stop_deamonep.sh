#!/bin/bash
Logep=$DIRBIN"/logep.sh"
pid=`pgrep 'deamonep.sh'`
if [ $? -eq 0 ]
then
	kill $pid
	echo "Se detiene la ejecucion de deamonep.sh bajo el pid=${pid}"
	$Logep "stop_deamonep" "INFO" "Se detiene la ejecución de deamonep.sh bajo el pid=${pid}"
	exit 0
else
	$Logep "stop_deamonep" "ERR" "Error: deamonep.sh no se estaba ejecutando"
	exit 1

fi
